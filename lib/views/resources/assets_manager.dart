
const String Image_Path = "assets/images";
const String Json_Path = "assets/json";
class ImageAssets {
  static const String splashLogo = "$Image_Path/splash_logo.png";
  static const String splash = "$Image_Path/splash.png";

  static const String rightArrowIc = "$Image_Path/right_arrow_ic.svg";
  static const String leftarrowIc = "$Image_Path/left_arrow_ic.svg";
  static const String solidCircleIc = "$Image_Path/solid_circle_ic.svg";
  static const String hollowCirlceIc = "$Image_Path/hollow_cirlce_ic.svg";
  static const String photoCameraIc = "$Image_Path/photo_camera_ic.svg";
}
class JsonAssetes{
  static const String error = "$Json_Path/error.json";
  static const String loading = "$Json_Path/loading.json";
  static const String empty = "$Json_Path/empty.json";
  static const String success = "$Json_Path/success.json";

}