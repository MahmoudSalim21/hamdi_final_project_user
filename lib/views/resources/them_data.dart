
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hamdi_final_project/views/resources/styles_manager.dart';
import 'package:hamdi_final_project/views/resources/values_manager.dart';

import 'color_manager.dart';
import 'font_manager.dart';

ThemeData getApplicationTheme() {
  return ThemeData(
    // main colors
      primaryColor: ColorManager.primary,
      primaryColorLight: ColorManager.lightPrimary,
      primaryColorDark: ColorManager.darkPrimary,
      disabledColor: ColorManager.grey1,
      splashColor: ColorManager.lightPrimary,
      // ripple effect

      // cardview theme
      cardTheme: CardTheme(
          color: ColorManager.white,
          shadowColor: ColorManager.grey,
          elevation: AppSize.s4
      ),

      // appbar theme
      appBarTheme: AppBarTheme(
          centerTitle: true,
          color: ColorManager.primary,
          elevation: AppSize.s4,
          shadowColor: ColorManager.lightPrimary,
          titleTextStyle: getRegularStyle(
              fontSize: FontSize.s16, color: ColorManager.white)
      ),
      // buttom theme
      buttonTheme: ButtonThemeData(
          shape: const StadiumBorder(),
          disabledColor: ColorManager.grey1,
          buttonColor: ColorManager.primary,
          splashColor: ColorManager.lightPrimary
      ),

      // elevated buttom theme
      elevatedButtonTheme: ElevatedButtonThemeData(
          style: ElevatedButton.styleFrom(
              textStyle: getRegularStyle(
                  color: ColorManager.white, fontSize: FontSize.s12),
              primary: ColorManager.primary,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(AppSize.s12),
              )
          )
      ),

      // text theme
      textTheme: TextTheme(
        displayLarge: getSemiStyle(
            color: ColorManager.darkGrey, fontSize: FontSize.s16),
        headlineLarge: getSemiStyle(
            color: ColorManager.darkGrey, fontSize: FontSize.s16),
        headlineMedium: getRegularStyle(
            color: ColorManager.darkGrey, fontSize: FontSize.s14),
        titleMedium: getMediumStyle(
            color: ColorManager.primary, fontSize: FontSize.s16),
        titleSmall: getRegularStyle(
            color: ColorManager.white, fontSize: FontSize.s16),
        bodyLarge: getRegularStyle(color: ColorManager.grey1),
        bodySmall: getRegularStyle(color: ColorManager.grey),
        bodyMedium: getRegularStyle(color: ColorManager.grey2, fontSize: FontSize.s12),

        labelSmall: getBoldStyle(color: ColorManager.primary, fontSize: FontSize.s12),
      ),

      // input decoration theme (text form field)
      inputDecorationTheme: InputDecorationTheme(

        /// content padding
        contentPadding: const EdgeInsets.all(AppPadding.p8),

        /// hint style
        hintStyle: getRegularStyle(
            color: ColorManager.grey, fontSize: FontSize.s14),

        /// label style
        labelStyle: getMediumStyle(
            color: ColorManager.grey, fontSize: FontSize.s14),

        /// error style
        errorStyle: getRegularStyle(color: ColorManager.error),

        /// enabledBorder style
        enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(
                color: ColorManager.grey, width: AppSize.s1_5),
            borderRadius: BorderRadius.all(Radius.circular(AppSize.s8))),

        /// focusedBorder style
        focusedBorder: OutlineInputBorder(
            borderSide: BorderSide(
                color: ColorManager.primary, width: AppSize.s1_5),
            borderRadius: BorderRadius.all(Radius.circular(AppSize.s8),
            )
        ),
        /// errorBorder style
        errorBorder: OutlineInputBorder(
            borderSide: BorderSide(
                color: ColorManager.error, width: AppSize.s1_5),
            borderRadius: BorderRadius.all(Radius.circular(AppSize.s8),
            )
        ),
        /// focusedErrorBorder style
        focusedErrorBorder: OutlineInputBorder(
            borderSide: BorderSide(
                color: ColorManager.primary, width: AppSize.s1_5),
            borderRadius: BorderRadius.all(Radius.circular(AppSize.s8),
            )
        ),

      )

  );
}