class AppString{
  static const username = "Username" ;
  static const usernameError = "Please enter your Username" ;
  static const password = "Password" ;
  static const passwordError = "Please enter your Password" ;
  static const login = "Login" ;
  static const register = "register" ;
  static const forgetPassword = "Forget password" ;
  static const regesterText = "Sign up" ;
  static const loading = "Loading..." ;
  static const success = "Success" ;
  static const String userNameInvalid = "User name should be at least 8 chars";
  static const String mobileNumberInvalid = "Mobile should be at least 10 digits";
  static const String passwordInvalid = "Password should be at least 6 chars";
  static const String alreadyHaveAccount = "Already have account ? Login";
  static const String mobilNumber = "Mobile Number";
  static const String profilePicture = "Profile Picture";
  static const String photoGallery = "Photo Gallery";
  static const String photoCamera = "Photo Camera";
  static const String details = "Details";
  static const String about = "About Store";
  static const String storeDetails = "Store details";
  static const products = "products";






  static const retryAgain = "Retry again" ;
  static const ok = "Ok" ;

  static const String emailHint = 'Email';
  static const String invalidEmail = "Email format is wrong";
  static const String resetPassword = "Reset Password";
  static const String home = "home";
  static const String notifications = "Notifications";
  static const String search = "Search";
  static const String settings = "Settings";

}