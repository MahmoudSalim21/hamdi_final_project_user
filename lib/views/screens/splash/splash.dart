
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:hamdi_final_project/views/resources/color_manager.dart';

import '../../../logic/controllers/auth_controller.dart';
import '../../../routes/routes.dart';
import '../../resources/assets_manager.dart';


class Splash extends StatefulWidget {
  @override
  _SplashState createState() => _SplashState();
}

class _SplashState extends State<Splash> {
  AuthController authController = Get.find();


  @override
  void initState() {
    Future.delayed(Duration(seconds: 3), (() {
      if (authController.isAuth()) {
        Get.offAndToNamed(AppRoutes.main_home);
      } else {
        Get.offAndToNamed(AppRoutes.login);
      }
    }));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: ColorManager.primary,
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        child:   Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: const [
              Center(child: Image(image: AssetImage(ImageAssets.splash),width: 200,)),
              Text(
                'Hamdi App',
                style: TextStyle(
                  fontSize: 30,
                  fontWeight: FontWeight.bold,
                  color: Colors.white,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
