
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:get/get_instance/src/extension_instance.dart';

import '../../logic/controllers/auth_controller.dart';
import '../../logic/controllers/home_view_controller.dart';
import '../../logic/controllers/side_bar_controller.dart';
import '../../routes/routes.dart';
import '../resources/color_manager.dart';

class MainHome extends StatelessWidget {
  AuthController authController = Get.find();
  OpenCloseDrawerController openCloseDrawerController = Get.find();
  // SendLocationController sendLocationController = Get.find();
  @override
  Widget build(BuildContext context) {
    return GetBuilder<HomeViewController>(
        init: HomeViewController(),
        builder: (controller) => Directionality(textDirection: TextDirection.rtl, child: Scaffold(
          key: openCloseDrawerController.scaffoldKey,
          appBar: AppBar(
            backgroundColor: ColorManager.primary,
            title: const Text("user App",
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    // fontSize: 33,
                     )),

            centerTitle: true,
            actions: <Widget>[
              IconButton(
                icon: Icon(Icons.notifications_none),
                onPressed: () {},
              )
            ],
          ),
          drawer: Drawer(
            child: ListView(
              children: [
                const Center(
                  child:  UserAccountsDrawerHeader(
                    accountEmail: Text('0992019123',style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 20,
                        fontFamily: 'Hadith' ,
                        color: Colors.white),),

                    accountName: Text('user name',style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 20,

                        color: Colors.white),),

                    currentAccountPicture: CircleAvatar(
                      backgroundImage:NetworkImage('https://www.maxpixel.net/static/photo/1x/User-People-Man-Business-Person-Avatar-Character-3637425.png')  ,
                    ),
                    decoration: BoxDecoration(
                        image: DecorationImage(image: NetworkImage('https://www.beautyppt.com/uploads/thumbnails/blue-hazy-blur-ppt-background-image.jpg') , fit: BoxFit.cover)
                    ),
                  ),
                ),
                ListTile(
                  onTap: () {},
                  title: const Text('main',
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 20,
                        fontFamily: 'Hadith' ,
                        color: Colors.black),
                  ),
                  leading: Icon(Icons.home , color: Colors.black,size: 25,),
                  trailing: Icon(Icons.arrow_forward_ios , color: Colors.black,size: 20,),
                ),
                ListTile(
                  onTap: () {},
                  title: const Text('setting',
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 20,
                          fontFamily: 'Hadith' ,
                      color: Colors.black),
                  ),
                  leading: Icon(Icons.settings , color: Colors.black,size: 25,),
                  trailing: Icon(Icons.arrow_forward_ios , color: Colors.black,size: 20,),
                ),
                ListTile(
                  onTap: () {},
                  title: const Text('info',
                      style: TextStyle(
                          fontWeight: FontWeight.bold, fontSize: 20)),
                  leading: Icon(Icons.view_week_outlined , color: Colors.black,size: 25,),
                  trailing: Icon(Icons.arrow_forward_ios , color: Colors.black,size: 20,),
                ),
                ListTile(
                  onTap: () {
                    Get.toNamed(AppRoutes.productDetails);
                    openCloseDrawerController.CloseDrawer();
                  },
                  title: const Text('about us',
                      style: TextStyle(
                          fontWeight: FontWeight.bold, fontSize: 20)),
                  leading: Icon(Icons.people_sharp , color: Colors.black,size: 25,),
                  trailing: Icon(Icons.arrow_forward_ios , color: Colors.black,size: 20,),
                ),
                Divider(height: 40,thickness: 2,),
                ListTile(
                  onTap: () {
                    authController.doLogout();
                  },
                  title: const Text('Log out',
                      style: TextStyle(
                          fontWeight: FontWeight.bold, fontSize: 20)),
                  leading: Icon(Icons.login_outlined , color: Colors.black,size: 25,),
                  trailing: Icon(Icons.arrow_forward_ios , color: Colors.black,size: 20,),
                ),
              ],
            ),
          ),
          body: controller.currentScreen,
          bottomNavigationBar: bottomNavigationBar(),
        )));
  }
  Widget bottomNavigationBar() {
    return GetBuilder<HomeViewController>(
      init: HomeViewController(),
      builder: (controller) => BottomNavigationBar(
        items: const [
          BottomNavigationBarItem(icon: Icon(Icons.perm_identity_sharp) , label:'profile'),
          BottomNavigationBarItem(icon: Icon(Icons.favorite_border_sharp) , label: 'favorite'),
          BottomNavigationBarItem(icon: Icon(Icons.shopping_cart_outlined) , label: 'orders'),
          BottomNavigationBarItem(icon: Icon(Icons.home_outlined) , label: 'home'),
        ],
        currentIndex: controller.navigatorValue,
        selectedItemColor: ColorManager.primary,
        unselectedItemColor: Colors.black,
        backgroundColor: Colors.grey.shade50,
        onTap: (index) {
          controller.changeSelectedValue(index);
        },
      ),
    );
  }
}
