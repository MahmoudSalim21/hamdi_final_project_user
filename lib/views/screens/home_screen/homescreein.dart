import 'package:carousel_slider/carousel_options.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:hamdi_final_project/database/models/product_model.dart';
import '../../../logic/controllers/product_controller.dart';
import '../../resources/color_manager.dart';
import '../../resources/strings_manager.dart';
import '../../resources/values_manager.dart';

class HomeScreen extends GetView {
  HomeScreen({Key? key}) : super(key: key);
  final ProductController productController = Get.find();

  @override
  Widget build(BuildContext context) {
    return Obx(() => productController.loading.value
        ? Center(child: CircularProgressIndicator())
        :  Scaffold(
        backgroundColor: Colors.white,
        body: SingleChildScrollView(
          child: _getContentWidget(),
        ),
      ),
    );
  }

  Widget _getContentWidget() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        _getProductCoursolWidget(productController.product),
        _getSection(AppString.products),
        // _getServicesWidget(snapshot.data?.services),
        // _getSection(AppString.stores),
        _getProductsWidget(productController.product),
      ],
    );
  }

  Widget _getProductCoursolWidget(List<Product>? products) {
    if (products != null) {
      return CarouselSlider(
          items: products
              .map((product) => SizedBox(
                    width: double.infinity,
                    child: Card(
                      // elevation: AppSize.s1_5,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(AppSize.s12),
                          side: BorderSide(
                              color: ColorManager.primary, width: AppSize.s1)),
                      child: Stack(
                        children: [
                          ClipRRect(
                            borderRadius: BorderRadius.circular(AppSize.s12),
                            child: Image.network(
                              'https://img.freepik.com/free-photo/front-view-burger-stand_141793-15542.jpg?size=626&ext=jpg',
                              fit: BoxFit.fill,
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.all(AppPadding.p8),
                            child: Align(
                                alignment: Alignment.bottomCenter,
                                child: Text(
                                  product.name!,
                                  style: TextStyle(
                                      color: ColorManager.white, fontSize: 20),
                                )),
                          ),
                        ],
                      ),
                    ),
                  ))
              .toList(),
          options: CarouselOptions(
              height: AppSize.s190,
              autoPlay: true,
              enableInfiniteScroll: true,
              enlargeCenterPage: true));
    } else {
      return Container();
    }
  }

  Widget _getSection(String title) {
    return Padding(
      padding: const EdgeInsets.only(
          top: AppPadding.p12,
          left: AppPadding.p12,
          right: AppPadding.p12,
          bottom: AppPadding.p2),
      child: Text(
        title,
      ),
    );
  }

  Widget _getProductsWidget(List<Product>? product) {
    if (product != null) {
      return Padding(
        padding: const EdgeInsets.only(
            left: AppPadding.p12, right: AppPadding.p12, top: AppPadding.p12),
        child: Flex(
          direction: Axis.vertical,
          children: [
            GridView.count(
              crossAxisCount: AppSize.s2,
              crossAxisSpacing: AppSize.s8,
              mainAxisSpacing: AppSize.s8,
              physics: const ScrollPhysics(),
              shrinkWrap: true,
              children: List.generate(product.length, (index) {
                return InkWell(
                  onTap: () {
                    // navigate to store details screen
                    // Navigator.of(context).pushNamed(Routes.storDetailRoute);
                  },
                  child: Stack(
                    children: [
                      Container(
                        height : 150,

                        child: Card(
                          elevation: AppSize.s4,
                          child: Image.network(
                            // product[index].image!,
                            "https://img.freepik.com/free-photo/beef-cotlet-burger-with-sauce-wooden-board_114579-2600.jpg?size=626&ext=jpg&ga=GA1.2.236579122.1657956296",
                            fit: BoxFit.fill,
                          ),
                        ),
                      ),
                      IconButton(
                          icon: const Icon(
                            Icons.favorite,
                            size: 30,
                          ),
                          color: Colors.grey.shade400,
                          onPressed: () {
                            print(product[index].name);
                          }),
                      Align(
                        alignment: Alignment.bottomCenter,
                        child: Text(product[index].price.toString()+"\$"),
                      )
                    ],
                  ),
                );
              }),
            )
          ],
        ),
      );
    } else {
      return Container();
    }
  }
}
