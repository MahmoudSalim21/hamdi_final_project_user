import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:get/get.dart';

import '../../../logic/controllers/login_controller.dart';
import '../../../routes/routes.dart';
import '../../resources/assets_manager.dart';
import '../../resources/strings_manager.dart';
import '../../resources/values_manager.dart';

class LoginView extends GetView<LoginController> {
  LoginView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: SafeArea(
          child: Padding(
            padding: const EdgeInsets.only(top: AppPadding.p20),
            child: Center(
                child: Form(
              key: controller.loginFormKey,
              autovalidateMode: AutovalidateMode.onUserInteraction,
              child: Column(
                children: [
                  Center(child: Image(image: AssetImage(ImageAssets.splash),width: 200,)),
                  SizedBox(
                    height: AppSize.s8,
                  ),
                  Padding(
                    padding: EdgeInsets.only(
                        left: AppPadding.p28, right: AppPadding.p28),
                    child: TextFormField(
                      controller: controller.emailController,
                      validator: (v) {
                        return controller.validatephone(v!);
                      },
                      onSaved: (v) {
                        controller.phone = v!;
                      },
                      keyboardType: TextInputType.emailAddress,
                      decoration: InputDecoration(
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10),
                        ),
                        labelText: "email",
                        prefixIcon: const Icon(Icons.email_outlined),
                      ),
                    ),
                  ),
                  const SizedBox(height: AppSize.s28,),
                  Padding(
                    padding: EdgeInsets.only(
                        left: AppPadding.p28, right: AppPadding.p28),
                    child: TextFormField(
                      controller: controller.passwordController,
                      validator: (v) {
                        return controller.validatePassword(v!);
                      },
                      onSaved: (v) {
                        controller.password = v!;
                      },
                      // keyboardType: TextInputType.phone,
                      // keyboardType: TextInputType.visiblePassword,
                      obscureText: true,
                      decoration: InputDecoration(
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10),
                        ),
                        labelText: "password",
                        prefixIcon: const Icon(Icons.password),
                      ),
                    ),
                  ),

                  Obx(
                    () => controller.isLoading.value == true
                        ? const Padding(
                          padding: EdgeInsets.all(10.0),
                          child: Center(
                              child: CircularProgressIndicator(),
                            ),
                        )
                        : const Text(''),
                  ),

                  SizedBox(
                    height: AppSize.s28,
                  ),
                  Padding(
                    padding: EdgeInsets.only(
                        left: AppPadding.p28, right: AppPadding.p28),
                    child: SizedBox(
                          width: double.infinity,
                          height: AppSize.s40,
                          child: ElevatedButton(
                              onPressed: () async {
                                await controller.doLogin();
                              },
                              child: Text(AppString.login)),
                        )
                  ),
                  Padding(
                      padding: EdgeInsets.only(
                          top: AppPadding.p8,
                          left: AppPadding.p28,
                          right: AppPadding.p28),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          TextButton(
                            onPressed: () {
                              // Navigator.pushNamed(
                              //     context, Routes.main_home);
                            },
                            child: Text(
                              AppString.forgetPassword,
                              style: Theme.of(context).textTheme.titleMedium,
                            ),
                          ),
                          TextButton(
                            onPressed: () {
                              // Navigator.pushNamed(
                              //     context, Routes.main_home);
                            },
                            child: Text(
                              AppString.regesterText,
                              style: Theme.of(context).textTheme.titleMedium,
                            ),
                          ),
                        ],
                      )),
                ],
              ),
            )),
          ),
        ),
      ),
    );
  }


}
