import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:hamdi_final_project/routes/routes.dart';
import 'package:hamdi_final_project/views/resources/color_manager.dart';
void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      title : "final project",
      theme: ThemeData(primarySwatch: Colors.amber),
      initialRoute: AppRoutes.splash,
      getPages: AppRoutes.routes ,
      debugShowCheckedModeBanner: false,
    );
  }
}

