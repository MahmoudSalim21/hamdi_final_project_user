import 'dart:convert';

ProductsModel productsModelFromJson(String str) =>
    ProductsModel.fromJson(json.decode(str));

String productModelToJson(ProductsModel product) => json.encode(product.toJson());

class ProductsModel {
  List<Product> data;

  ProductsModel({required this.data});

  factory ProductsModel.fromJson(Map<String, dynamic> json) => ProductsModel(
        data: List<Product>.from(
            json['response'].map((p) => Product.fromJson(p))),
      );

  Map<String, dynamic> toJson() =>
      {"data": List<Product>.from(data.map((d) => d.toJson()))};
}

class Product {
  int? id;
  String? name;
  int? price;
  String? image;
  bool? isActive;
  bool? isArchived;
  String? createdAt;
  String? updatedAt;
  int? BranchId;

  Product(
      {this.id,
      this.name,
      this.price,
      this.image,
      this.isActive,
      this.isArchived,
      this.createdAt,
      this.updatedAt,
      this.BranchId});

  factory Product.fromJson(Map<String, dynamic> json) => Product(
      id: json['id'],
      name: json['name'],
      price: json['price'],
      image: json['image'],
      isActive: json['isActive'],
      isArchived: json['isArchived'],
      createdAt: json['createdAt'],
      updatedAt: json['updatedAt'],
      BranchId: json['BranchId']);

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "price": price,
        "image": image,
        "isActive": isActive,
        "isArchived": isArchived,
        "createdAt": createdAt,
        "updatedAt": updatedAt,
        "BranchId": BranchId,
      };
}

