
import 'dart:convert';

LoginResponse userFromJson(String str) => LoginResponse.fromJson(json.decode(str));

class LoginResponse{
  Status? status ;
  Response? response ;

  LoginResponse({
    this.status,
    this.response
});

  factory LoginResponse.fromJson(Map<dynamic, dynamic> json) {
    return LoginResponse(
      status: Status.fromJson(json["status"]),
      response: Response.fromJson(json["response"])
    );
  }
}

class Status{
  int? code;
  String? message;

  Status({
    this.code,
    this.message
});

  factory Status.fromJson(Map<dynamic, dynamic> json) {
    return Status(
        code: json["code"],
        message: json["msg"]
    );
  }

}
class Response{
  Data? data;

  Response ({this.data});
  factory Response.fromJson(Map<dynamic, dynamic> json) {
    return Response(
        data: Data.fromJson(json["data"]),
    );
  }
}
class Data{
  String ? accessToken ;
  String ? refreshToken ;

  Data({this.accessToken, this.refreshToken});

  factory Data.fromJson(Map<dynamic, dynamic> json) {
    return Data(
      accessToken: json["accessToken"],
      refreshToken: json["refreshToken"],
    );
  }

}