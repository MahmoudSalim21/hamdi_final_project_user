import 'dart:convert';
import 'package:hamdi_final_project/database/models/product_model.dart';
import 'package:http/http.dart' as http;
import '../models/usr_model.dart';

class Services {
  static String baseApi= "http://86.106.131.129/api";
  static var client = http.Client();

  static login (email , password)async{
    var response = await client.post
      (Uri.parse("$baseApi/public/auth/user/login"),
    headers: {"Content-Type" : "application/json"},
    body : jsonEncode(<String, String>{ "email":email , "password" : password})
    );
    if (response.statusCode == 200){
      var stringObject = response.body;
      var user = userFromJson(stringObject);
      return user;
    }
    else{
      print('error status');
    }
  }

  static getProduct ()async{
    var response = await client.get
      (Uri.parse("$baseApi/public/product"),
    headers: {"Content-Type" : "application/json"},
    );
    if (response.statusCode == 200){
      var stringObject = response.body;
      var products = productsModelFromJson(stringObject);
      return products.data;
    }
    else{
      print('error status');
    }
  }



}
