
import 'package:get/get.dart';

import '../logic/bindings/auth_binding.dart';
import '../logic/bindings/just_login_binding.dart';
import '../logic/bindings/main_viewbinding.dart';
import '../views/screens/auth/login_view.dart';
import '../views/screens/home_screen/homescreein.dart';
import '../views/screens/main_home.dart';
import '../views/screens/splash/splash.dart';


class AppRoutes {
static const productDetails = Routes.productDetails;
static const login = Routes.login;
static const splash = Routes.splash;
static const home = Routes.home;
static const main_home = Routes.main_home;

  static final routes = [
    GetPage(name: Routes.login, page: () =>  LoginView() ,binding: JustLogin() ),
    GetPage(name: Routes.splash, page: () =>  Splash(),binding: AuthBinding()),
    GetPage(name: Routes.home, page: () =>  HomeScreen()),
    GetPage(name: Routes.main_home, page: () =>  MainHome(),binding:MainViewBinding()),
  ];

}
class Routes {
  static const productDetails = '/product_details';
  static const login = '/login';
  static const splash = '/splash';
  static const home = '/home';
  static const main_home = '/main_home';
}
