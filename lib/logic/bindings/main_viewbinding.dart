
import 'package:get/get_core/src/get_main.dart';
import 'package:get/get_instance/src/bindings_interface.dart';
import 'package:get/get_instance/src/extension_instance.dart';

import '../controllers/product_controller.dart';
import '../controllers/side_bar_controller.dart';

class MainViewBinding extends Bindings{
  @override
  void dependencies() {
    Get.lazyPut(() => OpenCloseDrawerController());
    Get.lazyPut(() => ProductController());

  }
}