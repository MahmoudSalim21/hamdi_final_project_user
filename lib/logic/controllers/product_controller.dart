

import 'package:get/get.dart';

import '../../database/models/product_model.dart';
import '../../database/services/services.dart';

class ProductController extends GetxController {
  RxBool loading = false.obs;
  RxInt index = 0.obs;



     List<Product> product = [];


  @override
  void onInit() async {
    print('product');
    await getallStudentCourse();
    super.onInit();
  }
  @override
  void dispose() {
    print("in dispose");
    index.value=0;

    super.dispose();
  }

  getallStudentCourse() async {
    loading.value = true;
    try {
      product.clear();
      product  = await Services.getProduct( );
      print(product);
    } catch (e) {
      print(e);
    }
    loading.value = false;
    update();
  }
}
