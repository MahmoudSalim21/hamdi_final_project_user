
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../views/screens/favorite/favorite.dart';
import '../../views/screens/home_screen/homescreein.dart';
import '../../views/screens/shopping_cart/shopping_cart.dart';
import '../../views/screens/profile/profile_view.dart';

class HomeViewController extends GetxController {

  int _navigatorValue = 3;

  get navigatorValue => _navigatorValue;

  Widget _currentScreen = HomeScreen();

  get currentScreen => _currentScreen;

  void changeSelectedValue(int selected) {
    _navigatorValue = selected;

    switch (selected) {
      case 0:
        {
          _currentScreen = ProfileView();
          break;
        }
      case 1:
        {
          _currentScreen = Favorite();
          break;
        }
      case 2:
        {
          _currentScreen =ShoppungCart ();
          break;
        }
      case 3:
        {
          _currentScreen =HomeScreen ();
          break;
        }
    }
    update();
  }
}
