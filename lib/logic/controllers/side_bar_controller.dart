import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get_state_manager/src/simple/get_controllers.dart';

class OpenCloseDrawerController extends GetxController {
  var scaffoldKey = GlobalKey<ScaffoldState>();

  void OpenDrawer(){
    scaffoldKey.currentState!.openDrawer();
  }

  void CloseDrawer(){
    scaffoldKey.currentState!.openEndDrawer();
  }


}