import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:get/get.dart';
import 'package:hamdi_final_project/database/services/services.dart';

import '../../database/models/usr_model.dart';
import '../../routes/routes.dart';

class LoginController extends GetxController {
  RxBool isLoading = false.obs;
  final loginFormKey = GlobalKey<FormState>();
  final storage = const FlutterSecureStorage();
  late TextEditingController emailController, passwordController;
  String phone = '', password = '';

  @override
  void onInit() {
    print("Constructor");
    emailController = TextEditingController();
    passwordController = TextEditingController();
    super.onInit();
  }

  @override
  void dispose() {
    print("in dispose");
    emailController.dispose();
    passwordController.dispose();
    super.dispose();
  }

  String? validatephone(String value) {
    if (!isEmailValid(value)) {
      return 'this is wrong';
    } else {
      return null;
    }
  }

  bool isEmailValid(String email) {
    return RegExp(
            r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
        .hasMatch(email);
  }

  String? validatePassword(String value) {
    if (value.length < 8) {
      return 'this is wrong';
    } else {
      return null;
    }
  }

  doLogin() async {
    bool isValidate = loginFormKey.currentState!.validate();
    if (isValidate) {
      isLoading.value = true;
      try {
        LoginResponse data = await Services.login(
            emailController.text, passwordController.text);
        // print(data.response!.data!.accessToken);
        await storage.write(key: 'token', value: data.response!.data!.accessToken);

        Get.offAndToNamed(AppRoutes.main_home);
      } catch (e) {
        print(e);
      } finally {
        isLoading.value = false;
      }

      // if (result.hasException) {
      //   print(result.exception);
      //
      //   if (result.exception!.graphqlErrors.isEmpty) {
      //     Get.snackbar('no connect', 'som thing wrong with server');
      //   } else {
      //     Get.snackbar('Exception', 'exception in some thing');
      //   }
      //   isLoading.value = false;
      // }
      // else {
      //   print(result.data!);
      //   var result1 = result.data!['authenticateUserWithPassword'];
      //   print("Task was successfully added");
      //
      //   if (result1['__typename'] == 'UserAuthenticationWithPasswordSuccess') {
      //     await storage.write(key: 'token', value: result1['sessionToken']);
      //     loginFormKey.currentState!.save();
      //     var token = (await storage.read(key: 'token'));
      //     print (token) ;
      //     Get.offAndToNamed(AppRoutes.main_home);
      //
      //   } else {
      //     Get.snackbar('login', 'problem in login');
      //   }
      //   isLoading.value = false;
      // }
    }
  }
}
