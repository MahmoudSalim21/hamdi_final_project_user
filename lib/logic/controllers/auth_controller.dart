import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:get/get.dart';

import '../../routes/routes.dart';



class AuthController extends GetxController {
  var storage = const FlutterSecureStorage();
  var name = ''.obs;
  var token = '';

  @override
  void onInit() async{
   await authData();
    super.onInit();
  }

  authData()async{
    if(await storage.read(key: 'token') != null){
      token = (await storage.read(key: 'token'))!;
    }
    else {
      token='';
    }
  }
  bool isAuth(){
    return token.isNotEmpty;
  }
  doLogout()async{
    await storage.deleteAll();
    Get.offAllNamed(Routes.login);
  }
}